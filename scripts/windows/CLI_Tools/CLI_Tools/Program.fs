﻿module Bootstrap.Main
// For more information see https://aka.ms/fsharp-console-apps


open System.Diagnostics.CodeAnalysis
open Argu
open Bootstrap.CLI.Setup
open Bootstrap.Servers.RPC
open FSharpPlus.Data
open Bootstrap.CLI.RunConfig
open Bootstrap.CLI.Arguments
open Bootstrap.Servers.ServerStratum
open Bootstrap.Servers.Start
open Bootstrap.Logging.DefaultLogSource
open ES.Fslog
open ES.Fslog.Loggers
open System

/// Given the parse results, build a 'RunConfig'
[<RequiresUnreferencedCode("buildRunConfig requires reflection")>]
let buildRunConfig (results: ParseResults<Arguments>) sink : RunConfig =
  let logProvider = new LogProvider()

  // create the log source
  let logSource = defaultLogSource logProvider

  // add all the loggers
  (results, logProvider, logSource, sink)
  |> AddNetworkSlave.addNetworkSlave
  |> AddOutFile.addOutfile
  |> fun(r, lp, ls) ->
    lp.AddLogger(ConsoleLogger(Utils.getLogLevel results))
    (r, lp, ls)

  |> ignore

  // get the stratum from the results
  let stratum: ServerStratum = results.GetResult(Stratum, Master)
  let runningAs = System.Environment.UserName
  let machineName = System.Environment.MachineName
  let skipProp: bool = results.TryGetResult SkipProp |> Option.isSome
  let masterAddr, operatingPort =
    results.TryGetResult Listener |> Option.defaultValue ("", 42069)
  // Return the RunConfig
  { log = logSource
    _logProvider = logProvider
    stratum = stratum
    runningAs = runningAs
    machineName = machineName
    masterAddr = masterAddr
    operatingPort = operatingPort
    skipProp = skipProp }

/// After parsing the args, continue here.
let runArgs (results: ParseResults<Arguments>) =
  let sink = UninitializedNetworkClientPacketSink()
  let runConfig = buildRunConfig results sink
  printfn $"results: %A{results}\nrunConfig: %A{runConfig}"
  // finally, run the program in the reader environment
  let _ = Reader.run (startServer sink) runConfig
  ()

[<EntryPoint>]
let main args =
  #if DEBUG
  let mutable acceptDanger = false
  let mutable firstPrint = true
  while not acceptDanger do
    if firstPrint then
      Console.WriteLine "WARNING: This is a debug build. You should only proceed if you\n
      don't care about potentially bricking this machine. Do you accept this risk?"
      firstPrint <- false
    Console.Write "Please respond [Y/N]: "
    let input = Console.ReadLine()
    match input.ToLower() with
      | "y" ->
        acceptDanger <- true
      | "n" ->
        exit 0
      | _ ->
        Console.WriteLine "Please answer Y/N."
  #endif
  let parser = ArgumentParser.Create<Arguments>(programName = "Bootstrap.exe")
  try
   parser.ParseCommandLine(inputs = args, raiseOnUsage = true) |> runArgs
   0
   with :? ArguParseException as e -> eprintfn $"%s{e.Message}"; 1
