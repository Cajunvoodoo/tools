﻿module Bootstrap.CLI.Setup.Utils

open Argu
open Bootstrap.CLI.Arguments
open ES.Fslog

/// The default log level, used when there is no specified logging level.
let defaultLogLevel = LogLevel.Informational

/// Get the log level from the CLI argument parsing results.
let getLogLevel (results: ParseResults<Arguments>) : LogLevel =
  results.GetResult(Verbosity, defaultValue = LogLevel.Informational)
