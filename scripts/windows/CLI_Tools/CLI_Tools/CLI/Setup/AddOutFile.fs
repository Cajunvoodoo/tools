﻿module Bootstrap.CLI.Setup.AddOutFile

open Argu
open Bootstrap.CLI.Arguments
open Bootstrap.CLI.Setup.Utils
open ES.Fslog
open ES.Fslog.Loggers

/// Add outfile listener to the 'LogProvider'
let addOutfile
  (
    results: ParseResults<Arguments>,
    logProvider: LogProvider,
    logSource: LogSource
  ) : ParseResults<Arguments> * LogProvider * LogSource =
  let outfile = results.GetResult(OutFile, "out.txt")
  let logLevel = getLogLevel results

  logProvider.AddLogger(new FileLogger(logLevel, outfile))
  
  (results, logProvider, logSource)
