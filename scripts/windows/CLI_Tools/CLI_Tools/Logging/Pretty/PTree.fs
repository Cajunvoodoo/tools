﻿module rec Bootstrap.Logging.Pretty.PTree


/// The PTree ("Pretty Tree") is a General Tree. Used for pretty printing.
type PTree<'A> (root : 'A, children : 'A PTree list) =
    new (root) = PTree(root, [||])
    new (root, [<System.ParamArray>]children) = PTree(root, List.ofArray children)
    member this.Root = root
    member this.Children = children
    
    member private t.ToLines format: string seq =
      seq {
        yield "+) " +  sprintf format t.Root

        if t.Children.Length > 0 then
          let cLines (joinerStr, terminatorStr) (tree: PTree<'A>) =
            seq {
              let cLines = Array.ofSeq (tree.ToLines format)
              yield joinerStr + cLines[0]
              for cl in Seq.skip 1 cLines -> terminatorStr + cl
            }

          for i = 0 to t.Children.Length - 2 do
            yield! cLines ("├─", "│  ") t.Children[i]

          yield! cLines ("└─", "  ") t.Children[t.Children.Length - 1]
      }
      
    override t.ToString(): string =
      let mutable ret = ""
      // strings have quotes around them with %A, this removes them
      let format = Printf.StringFormat<'A -> string>(if typeof<'A> = typeof<string> then "%s" else "%A")
      for line in t.ToLines format do
        ret <- $"{ret}\n{line}"
      ret
        
/// A pretty-printed tree with any amount of children. Shorthand for PTree.
type P<'A> = PTree<'A>
