﻿module Bootstrap.Logging.LogEventSerialize

open System.IO
open System.Runtime.Serialization
open System.Xml
open System.Xml.Serialization
open ES.Fslog


// NOTE: deserialization is very dangerous. Hopefully this doesn't bite us
// in the ass later. I imagine its safe, seeing as we reach out directly to the
// master, who spawned us. That is, we don't make any other network connections
// after the initial one, so an attacker shouldn't be able to insert arbitrary
// TCP traffic

/// Serialize a LogEvent into XML, writing it to the provided network stream
/// asynchronously. The provided logSource is for debug logging, as it is likely
/// local logging will be required
(*
let serializeToXML
  (logEvent: 'T)
  (stream: Stream)
  (logSource: LogSource)
  : Async<unit> =
  async {
    let serializer = new DataContractSerializer(typeof<'T>)

    // Serialize to network stream
    do! async { serializer.WriteObject(stream, logEvent) }

    // Serialize to verbose writer
    do! async {
      // we need a stream to store the stdout output
      use verboseStream = new MemoryStream()
      serializer.WriteObject(verboseStream, logEvent)
      // actually log the string
      // this is for logging
      use verboseReader = new StreamReader(verboseStream)
      verboseStream.Seek(0, SeekOrigin.Begin) |> ignore // not sure why this is required
      logSource?NetVerbose $"Serialized: {verboseReader.ReadToEnd()}"
    }
    return ()
  }
  
// Function to deserialize XML from a stream to a specified type.
let deserializeFromXml<'T> (stream: Stream): Option<'T>  =
    try
      let serializer = new DataContractSerializer(typeof<'T>)
      use reader = XmlReader.Create(stream)
      let ret = serializer.ReadObject(reader) :?> 'T
      printfn $"{ret}"
      Some ret
    with
    | _ -> None

    *)
