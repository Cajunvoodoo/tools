﻿module Bootstrap.Servers.Hardening.Serivces

open System.Runtime.InteropServices
open System
open Bootstrap.Utils.Utils
open System.ServiceProcess
open Microsoft.FSharp.NativeInterop
open Windows.Win32
open Windows.Win32.System.Services
open Powershell

(*
/// Win32 constants
/// Used for a few things, like setting the service type.
[<Literal>]
let SERVICE_NO_CHANGE: uint32 = 0xFFFFFFFFu

/// Service types
[<Literal>]
let SERVICE_FILE_SYSTEM_DRIVER = 0x00000002u
[<Literal>]
let SERVICE_KERNEL_DRIVER = 0x00000001u
[<Literal>]
let SERVICE_WIN32_OWN_PROCESS = 0x00000010u
[<Literal>]
let SERVICE_WIN32_SHARE_PROCESS = 0x00000020u

/// Service start types
[<Literal>]
let SERVICE_AUTO_START = 0x00000002u
[<Literal>]
let SERVICE_BOOT_START = 0x00000000u
[<Literal>]
let SERVICE_DEMAND_START = 0x00000003u
[<Literal>]
let SERVICE_DISABLED = 0x00000004u
[<Literal>]
let SERVICE_SYSTEM_START = 0x00000001u
 *)

type ChangeServiceConfigArgs =
  { serviceHandle: SafeHandle
    serviceType: ENUM_SERVICE_TYPE
    startType: SERVICE_START_TYPE
    errorControl: SERVICE_ERROR
    binaryPathName: string
    loadOrderGroup: string
    tagId: nativeptr<uint32>
    dependencies: string
    serviceStartName: string
    password: string
    displayName: string
  }
  with
  static member Default (serviceHandle: SafeHandle) =
    { serviceHandle = serviceHandle
      serviceType = ENUM_SERVICE_TYPE.SERVICE_NO_CHANGE
      startType = SERVICE_START_TYPE.SERVICE_NO_CHANGE
      errorControl = SERVICE_ERROR.SERVICE_NO_CHANGE
      binaryPathName = null
      loadOrderGroup = null
      tagId = NativePtr.nullPtr<uint32>
      dependencies = null
      serviceStartName = null
      password = null
      displayName = null
    }
/// Wrapper around `advapi32.dll ChangeServiceConfigW`. Preferable to using
/// the raw function
let ModifyService (
  {
    serviceHandle = serviceHandle;
    serviceType = serviceType;
    startType = startType;
    errorControl = errorControl;
    binaryPathName = binaryPathName;
    loadOrderGroup = loadOrderGroup;
    tagId = tagId;
    dependencies = dependencies;
    serviceStartName = serviceStartName;
    password = password;
    displayName = displayName
    }
  ): Result<unit, string> =
  let res = PInvoke.ChangeServiceConfig(
      serviceHandle,    // hService
      serviceType,      // dwServiceType
      startType,        // dwStartType
      errorControl,     // dwErrorControl
      binaryPathName,   // lpBinaryPathName
      loadOrderGroup,   // lpLoadOrderGroup
      tagId,            // lpdwTagId
      dependencies,     // lpDependencies
      serviceStartName, // lpServiceStartName
      password,         // lpPassword
      displayName       // lpDisplayName
    )
  if (!> res) then Ok()
  else
    let err = Marshal.GetLastWin32Error()
    Error (sprintf "Failed advapi32.dll::ChangeServiceConfigW.\nError code: %d" err)

/// The action to take on a service.
type ServiceAction =
  | Start
  | Stop
  | Restart

/// Whether the service is currently running or not.
type ServiceStatus =
  | Running
  | Disabled

(*
/// The startup mode a particular service has.
/// Note that due to complexity with advapi32.dll, AutomaticDelayed is not supported.
type ServiceStartupMode =
  | Automatic = SERVICE_AUTO_START
  | Manual = SERVICE_DEMAND_START
  | Disabled = SERVICE_DISABLED
*)

/// Set the startup mode of a service.
let setStartupMode (serviceName: string) (mode: SERVICE_START_TYPE): Result<unit, string> =
  use service = new ServiceController(serviceName)
  let res = ModifyService(
    {ChangeServiceConfigArgs.Default(service.ServiceHandle) with startType = mode})
  service.Refresh()
  res


let DisablePrintSpooler (): PwshResult =
    let command = @"
      Set-Service -ServiceName Spooler -StartupType Disabled
      Stop-Service -Name Spooler
    "
    RunPowershellScript command FailOnOutput 5000
    
let RegisterHardenServices(): (string * ScriptCb * list<WindowsVariant>) =
  "HardenServices-Any", DisablePrintSpooler, AnyVariant
