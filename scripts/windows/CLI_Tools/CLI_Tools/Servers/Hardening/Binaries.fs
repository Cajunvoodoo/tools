﻿module Bootstrap.Servers.Hardening.Binaries

open Powershell


/// Enable DEP/NX in the Bootloader.
let EnableDepInBootloader(): PwshResult =
  let command = @"
  bcdedit /set nx AlwaysOn
  "
  RunPowershellScript command AnyCode 5000

 
/// Disable "Test Mode" in the Bootloader.
let DisableTestModeInBootloader(): PwshResult =
  let command = @"
  bcdedit /set testsigning off
  "
  RunPowershellScript command AnyCode 5000

/// Enable Driver Signature Enforcement in the Bootloader.
let EnableDriverSignatureEnforcementInBootloader(): PwshResult =
  let command = @"
  bcdedit /set nointegritychecks off
  "
  RunPowershellScript command AnyCode 5000
  
let RegisterBinaryAndDriverHardening(): (string * ScriptCb * list<WindowsVariant>) =
  let cb() =
    [ EnableDepInBootloader()
      DisableTestModeInBootloader()
      EnableDriverSignatureEnforcementInBootloader()
    ]
    |> MergePwshResults
  "BootloaderHardening-Any", cb, AnyVariant
