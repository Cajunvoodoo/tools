﻿module Bootstrap.Servers.Master.LoggingServer

open Bootstrap.Servers.Master.Server
open System
open Bootstrap.CLI
open Bootstrap.CLI.RunConfig
open Bootstrap.Servers.RPC
open FSharpPlus.Data
open FSharpPlus
open WatsonTcp
open ES.Fslog

(*
/// Listen for our slave and log their messages
type LoggingServer private (Config: RunConfig, tcpServer: WatsonTcpServer) =
  // NOTE: we keep a private RunConfig because these methods will be
  // run asynchronously and 'autonomously'.
  inherit Server(tcpServer)

  static member Create(port: int): Reader<RunConfig, LoggingServer> =
    monad' {
      let! env = ask
      env.log?NetWarn "Creating non-TLS logging listener. This should probably be fixed!"
      env.log?NetInfo $"Creating new TcpServer listening on 0.0.0.0:{port}"
      let server: WatsonTcpServer = new WatsonTcpServer("0.0.0.0", port)
      return LoggingServer(env, server)
    }

  member this.Start() =
    Config.log?NetInfo "Starting Logging Server on port"
    this.Server.Start()

  // TODO: logging related to client connecting and disconnecting

  /// Logs the message to the appropriate logger, using the metadata 'type'
  /// to determine the LogLevel.
  override this.MessageReceived(eventArgs: MessageReceivedEventArgs) =
    let urgency = enum<RpcLo>(eventArgs.Metadata["type"] :?> int)
    let message = System.Text.Encoding.Default.GetString(eventArgs.Data)
    let log = Config.log
    match urgency with
    | LogRPC.RLCritical -> log?SlaveCritical message
    | LogRPC.RLErr -> log?SlaveErr message
    | LogRPC.RLWarn -> log?SlaveWarn message
    | LogRPC.RLInfo -> log?SlaveInfo message
    | LogRPC.RLVerbose -> log?SlaveVerbose message
    | _ -> System.ArgumentOutOfRangeException() |> raise
*)