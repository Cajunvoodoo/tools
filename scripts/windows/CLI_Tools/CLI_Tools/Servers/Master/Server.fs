﻿module Bootstrap.Servers.Master.Server

open System
open Bootstrap.CLI
open Bootstrap.CLI.RunConfig
open Bootstrap.Servers.Master.PortFactory
open FSharpPlus.Data
open FSharpPlus
open WatsonTcp
open ES.Fslog
open System.Net

[<AbstractClass>]
type Server(server: WatsonTcpServer) =
  let mutable InternalWhitelistedNames: string seq = [||]

  member val Server = server with get
  // member val WhitelistedNames: string array = [||] with get
  member this.WhitelistedNames
    with get (): string seq = InternalWhitelistedNames
    and set (names: string seq) = 
      InternalWhitelistedNames <- names
      names 
      |> Seq.map (fun name -> 
        let _ipEntries: IPAddress array = Dns.GetHostAddresses name
        let mutable ipEntry: IPAddress = null
        if _ipEntries.Length = 1 then
          ipEntry <- _ipEntries.[0]
        else ipEntry <- _ipEntries.[1] // If we can't resolve the name, we'll crash
        ipEntry.ToString())
      |> fun ipAddrs ->
        server.Settings.PermittedIPs <- Collections.Generic.List(ipAddrs)

  /// Start the server asynchronously, attaching the listeners to the server
  member this.Start(): unit =
    // Connected
    server.Events.ClientConnected |> Event.add this.ClientConnected
    // Disconnected
    server.Events.ClientDisconnected |> Event.add this.ClientDisconnected
    // Messages
    server.Events.MessageReceived |> Event.add this.MessageReceived

    server.Start()

  /// Event handler for a received message
  abstract member MessageReceived: MessageReceivedEventArgs -> unit

  /// Internal method for adding behavior (e.g., logging) to the connection event
  abstract member ClientConnected: ConnectionEventArgs -> unit

  default this.ClientConnected(eventArgs: ConnectionEventArgs) = ()

  /// Internal method for adding behavior (e.g., logging) to the disconnection event
  abstract member ClientDisconnected: DisconnectionEventArgs -> unit

  default this.ClientDisconnected(eventArgs: DisconnectionEventArgs) = ()
