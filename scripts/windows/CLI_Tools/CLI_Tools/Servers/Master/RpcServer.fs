﻿module Bootstrap.Servers.Master.RpcServer

open System
open System.Collections.Generic
open System.Text.Json
open Bootstrap.CLI.RunConfig
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.Master.Server
open Bootstrap.Servers.Master.IRpcReceiver
open Bootstrap.Servers.Generic
open Bootstrap.Servers.RPC
open FSharpPlus.Data
open FSharpPlus
open WatsonTcp
open ES.Fslog


/// The RpcServer is used by the master to manage and distribute messages
/// partaining to specific Slaves to the models representing those Slaves.
/// Slaves have something similar in Servers.Slave.RpcClient, but the client
/// doesn't have a model-based representation (or, rather, is *just* the model,
/// but it won't be used for visualizations).
type RpcServer
  private
  (
    Config: RunConfig,
    tcpServer: WatsonTcpServer,
    // Guid is MD5 hash of String name
    SlaveMap: SortedDictionary<String * Guid, IRpcReceiver>
  ) =
  inherit Server(tcpServer)

  /// Create a new RpcServer managing the specified computers.
  static member Create
    (
      _rpcReceiversStr: SortedDictionary<String, IRpcReceiver>,
      port: int
    ) : Reader<RunConfig, RpcServer> =
    monad' {
      let! env = ask
      // TODO: TLS for RpcServer
      env.log?NetWarn
        "Creating non-TLS RPC listener. This should probably be fixed!"

      env.log?NetInfo $"Creating new TcpServer listening on 0.0.0.0:{port}"
      let server: WatsonTcpServer = new WatsonTcpServer("0.0.0.0", port)
      // make a new dictionary with the guid attached
      let rpcReceivers: SortedDictionary<String * Guid, IRpcReceiver> = SortedDictionary<_,_>()
      for name in _rpcReceiversStr.Keys do
        let guidName = CalculateGuid name
        let receiver = _rpcReceiversStr[name]
        rpcReceivers.Add(guidName, receiver)
      return RpcServer(env, server, rpcReceivers)
    }

  // member this.Start() =
  //   Config.log?NetInfo "Starting Logging Server on port"
  //   this.Server.Start()

  override this.MessageReceived(eventArgs: MessageReceivedEventArgs) =
    if not (eventArgs.Metadata.ContainsKey("name") && eventArgs.Metadata.ContainsKey("type")) then
      P("RpcServer: received message without name and/or type",
        P $"Connection Guid: {eventArgs.Client.Guid}")
      |> Config.log?SlaveErr
    else
      //let slaveName = System.Text.Encoding.Default.GetString(eventArgs.Metadata["name"] :?> byte[])
      let slaveName = eventArgs.Metadata["name"] :?> JsonElement |> _.GetString()
      let msgType: RpcPrimaryType =
        eventArgs.Metadata["type"] :?> JsonElement
        |> _.GetInt32()
        |> enum<RpcPrimaryType>
      // let msgType = enum<RpcPrimaryType>(_msgType.GetInt32())
      Config.log?SlaveVerbose $"RpcServer: Received message from {slaveName}"

      try
        // SlaveMap
        // |> Map.find (CalculateGuid slaveName)
        SlaveMap[CalculateGuid slaveName]
        |> fun receiver ->
          match msgType with
            | RpcPrimaryType.Control ->
              Config.log?SlaveErr "RpcServer: Received Control RPC from a Slave. Slaves should send Logging RPC, rather than Control RPCs"
              // Config.log?SlaveVerbose "RpcServer: Attempting to deserialize Control message..."
              // When dealing with Rpcs, the server does not *really* care about what Rpc
              // they are sending. Therefore, the Rpc we are responding to is sent back in
              // metadata. If it isn't present, we also don't really care. The data is simply a
              // string.
              // let rpc: ControlRpc = DecodeRpc (eventArgs.Metadata["rpc"] :?> byte[])
              // let rpc: ControlRpc = DecodeRpc eventArgs.Data
              // let rpcResponse: string = System.Text.Encoding.Default.GetString(eventArgs.Data)

              // Config.log?SlaveVerbose "RpcServer: Successfully deserialized"
              // receiver.ReceiveControlMessage rpc rpcResponse

            | RpcPrimaryType.Logging ->
              Config.log?SlaveVerbose "RpcServer: Attempting to deserialize Logging message..."
              let rpc: LogRpc = DecodeRpc eventArgs.Data
              Config.log?SlaveVerbose "RpcServer: Successfully deserialized"
              // NOTE: logging the received logs is *very* spammy. It *must* be
              //       set to verbose. It's also very confusing, since it looks
              //       the same as our own logs.
              P ("RpcServer: Received log from Slave",
                 P $"Slave: {slaveName}",
                 P $"Log: %A{rpc}")
              |> Config.log?SlaveVerbose
              receiver.ReceiveLogMessage rpc
            | RpcPrimaryType.Hello ->
              let hello: HelloRpc = DecodeRpc eventArgs.Data
              receiver.ReceiveHelloMessage hello
              P ("RpcServer: Received HELLO from Slave",
                 P $"Slave: {slaveName}",
                 P $"WinVer: {hello.windowsVersion}")
              |> Config.log?SlaveInfo
            | _ ->
              let msgTypeInt: int32 = eventArgs.Metadata["type"] :?> JsonElement |> _.GetInt32()
              P ("RpcServer: Received message who's type is out of range",
                 P $"Connection Guid: {eventArgs.Client.Guid}",
                 P $"Received value: {msgTypeInt}")
              |> Config.log?SlaveErr
      with
      | :? KeyNotFoundException as ex ->
        P("RpcServer: Received message from untracked Slave, or the Rpc in a response was not found",
          P $"Slave: {slaveName}",
          P $"Connection Guid: {eventArgs.Client.Guid}",
          P $"Exception: {ex.Message}")
        |> Config.log?SlaveErr
      | ex ->
        P("RpcServer: An exception occurred while receiving message",
          P $"Slave: {slaveName}",
          P $"Connection Guid: {eventArgs.Client.Guid}",
          P $"Exception: {ex.Message}")
        |> Config.log?SlaveErr


  override this.ClientConnected(eventArgs: ConnectionEventArgs) =
    try
      // construct the name, guid pair and find the respective receiver
      // to send the Connected event to
      for _name, _guid in SlaveMap.Keys do
        if _guid = eventArgs.Client.Guid then
          let receiver: IRpcReceiver = SlaveMap[_name, _guid]
          receiver.Connected eventArgs
    with
    | :? KeyNotFoundException as ex ->
      P("RpcServer: Unknown client connected",
        P $"Connection Guid: {eventArgs.Client.Guid}",
        P $"Exception: {ex.Message}")
      |> Config.log?SlaveErr

  override this.ClientDisconnected(eventArgs: DisconnectionEventArgs) =
    try
      for _name, _guid in SlaveMap.Keys do
        if _guid = eventArgs.Client.Guid then
          let receiver: IRpcReceiver = SlaveMap[_name, _guid]
          receiver.Disconnected eventArgs
    with
    | :? KeyNotFoundException as ex -> ()
      // NOTE: It appears setting GUID for disconnection events may be buggy.
      //       Therefore, we cannot rely on correct GUIDs for disconnection events.
      // WTF. This means a client *we don't know* disconnected. This should
      // never happen! Therefore, we bail out with a critical error.
      // P("RpcServer: Received disconnection from unknown client. This should never happen!",
      //   P $"Connection Guid: {eventArgs.Client.Guid}",
      //   P $"Client IP: {eventArgs.Client.IpPort}",
      //   P $"Reason: {eventArgs.Reason}",
      //   P $"Exception: {ex.Message}",
      //   P $"Known Slaves: %A{SlaveMap.Keys}"
      //   )
      // |> Config.log?SlaveCritical
      // System.Threading.Thread.Sleep(500) // Wait for logging to flush to network
      // exit -666

  interface IRpcAcceptor with
    member this.BroadcastRpc(rpc: ControlRpc): unit =
      raise (System.NotImplementedException())
    member this.RegisterController(computerName: string) (rpcReceiver: IRpcReceiver): unit =
      this.WhitelistedNames <- List.append (Seq.toList this.WhitelistedNames) [computerName]
      // TODO: what else to do here?

    member this.SendRpc (rpc: ControlRpc) (computerName: string) =
      let (_, guid) = CalculateGuid computerName
      let md: Dictionary<string, Object> = new Dictionary<string, Object>()
      md.Add("type", int(RpcPrimaryType.Control))
      md.Add("name", System.Environment.MachineName)

      let data = EncodeRpc rpc

      task {
        let! res = this.Server.SendAsync(guid, data, md)
        if not res then
          P("RpcServer: Failed to send Rpc to client",
            P $"Client: {computerName}",
            P $"Connection Guid: {guid}",
            P $"Rpc: {rpc}")
          |> Config.log?SlaveErr
        else
          P("RpcServer: Successfully sent Rpc to client",
            P $"Client: {computerName}",
            P $"Connection Guid: {guid}",
            P $"Rpc: {rpc}",
            P $"Note: reply not yet received")
          |> Config.log?SlaveVerbose
      } |> ignore
