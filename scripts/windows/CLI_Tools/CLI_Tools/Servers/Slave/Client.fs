﻿module Bootstrap.Servers.Slave.Client

open WatsonTcp
open Bootstrap.Servers.Generic

/// Client with a specific GUID determined by its name.
[<AbstractClass>]
type Client(client: WatsonTcpClient, clientName: string) =
  do
    client.Settings.Guid <- snd <| CalculateGuid clientName

  member val Client = client with get

  member this.Start(): unit =
    client.Events.ServerConnected |> Event.add this.ServerConnected
    client.Events.ServerDisconnected |> Event.add this.ServerDisconnected
    client.Events.MessageReceived |> Event.add this.MessageReceived

    client.Connect()

  /// Internal method called when connection to the server is established.
  abstract member ServerConnected: eventArgs: ConnectionEventArgs -> unit

  /// Internal method called when connection to the server is lost.
  abstract member ServerDisconnected: eventArgs: DisconnectionEventArgs -> unit

  /// Internal method specifying how to react to a received message.
  abstract member MessageReceived: eventArgs: MessageReceivedEventArgs -> unit

