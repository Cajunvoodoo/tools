﻿module Bootstrap.Servers.Slave.RpcClient

open System
open System.Collections.Generic
open System.Text
open System.Text.Json
open Bootstrap.CLI.RunConfig
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.RPC
open Bootstrap.Servers.Slave.Client
open WatsonTcp
open ES.Fslog
open Win32_WinRT_Bindings

/// The primary implementation of <c>Client</c> for the Slaves.
/// Actions performed by the slaves are sent back to the Master for logging.
type RpcClient
  private
  (
    config: RunConfig,
    client: WatsonTcpClient
  ) =
  inherit Client(client, config.machineName)

  /// Create a new RpcClient
  static member Create
    (
      config: RunConfig
    ) : RpcClient =
    let client = new WatsonTcpClient(config.masterAddr, config.operatingPort)
    RpcClient(config, client)

  override this.ServerConnected(eventArgs: ConnectionEventArgs) =
    try
      // NOTE: `eventArgs.Client` is Null!
      // send a Hello to the server
      config.log?SlaveInfo "RpcClient: Connected to master"

      let metadata: Dictionary<string, obj> = Dictionary()
      metadata.Add("name", config.machineName)
      metadata.Add("type", int(RpcPrimaryType.Hello))
      let hello: HelloRpc = {
        windowsVersion = $"{OSVersionInfo.Name} {OSVersionInfo.Edition} ({OSVersionInfo.Version})"
      }
      let data = EncodeRpc hello
      client.SendAsync(data, metadata) |> ignore
      
      P ("RpcClient: Sent Hello Message",
         P $"Master: {config.masterAddr}")
      |> config.log?SlaveVerbose
    with
    | ex ->
      P ("RpcClient: Uncaught exception in ServerConnected",
         P $"Reason: {ex.Message}",
         P $"Stacktrace:\n{ex.StackTrace}")
      |> config.log?SlaveErr

  override this.ServerDisconnected(eventArgs: DisconnectionEventArgs) =
    try 
      P ("RpcClient: Connection lost with Master",
         P $"Master: {config.masterAddr}",
         P $"Reason: {eventArgs.Reason}")
      |> config.log?SlaveCritical
      // FIXME: Should we exit when the connection with the Master is lost?
      //        It makes the iteration process easier, that's for sure.
      exit -1 
    with
    | ex -> 
      P ("RpcClient: Uncaught exception in ServerDisconnected",
         P $"Reason: {ex.Message}",
         P $"Stacktrace:\n{ex.StackTrace}")
      |> config.log?SlaveErr

  /// Reply to the Master after completing an RPC.
  member private this.Reply(rpc: LogRpc): unit =
    let metadata: Dictionary<string, obj> = Dictionary()
    metadata.Add("name", config.machineName)
    metadata.Add("type", int(RpcPrimaryType.Logging))
    let data = EncodeRpc rpc
    let timestamp = DateTime.Now
    P ("RpcClient: Sending message",
       P $"Timestamp: %A{timestamp}",
       P $"Metadata: %A{metadata}",
       P $"LogRpc: %A{rpc}",
       P $"Encoded: {Encoding.Default.GetString data}")
    |> config.log?SlaveInfo
    task {
      let! res = client.SendAsync(data, metadata)
      if not res then
        P ("RpcClient: Failed to send message",
           P $"Sent at: %A{timestamp}",
           P $"LogRpc: %A{rpc}")
        |> config.log?SlaveErr
    } |> ignore

  override this.MessageReceived(eventArgs: MessageReceivedEventArgs) =
    try 
      if not (eventArgs.Metadata.ContainsKey("name") && eventArgs.Metadata.ContainsKey("type")) then
        P("RpcClient: received message without name and/or type",
          P $"Connection Guid: {eventArgs.Client.Guid}")
        |> config.log?SlaveErr

      let sender = eventArgs.Metadata["name"]
      let msgType = eventArgs.Metadata["type"]
      P ("RpcClient: Received message",
         P $"Sender: {sender}",
         P $"Message Type: {msgType}",
         P $"Metadata: %A{eventArgs.Metadata}",
         P $"Contents: {Encoding.Default.GetString eventArgs.Data}")
      |> config.log?SlaveVerbose
      let _metadataType = eventArgs.Metadata["type"] :?> JsonElement
      match enum<RpcPrimaryType>(_metadataType.GetInt32()) with
      | RpcPrimaryType.Hello -> config.log?SlaveErr "RpcClient: Slaves should not receive Hello RPCs."
      | RpcPrimaryType.Logging -> config.log?SlaveErr "RpcClient: Slaves should not receive Logging RPCs."
      | RpcPrimaryType.Control ->
        let rpc: ControlRpc = DecodeRpc eventArgs.Data
        match rpc with
        | Ping ->
          let reply = RpcData(rpc, "Pong!")
          this.Reply reply
        | _ -> config.log?SlaveErr $"RpcClient: Unimplemented ControlRpc: {rpc}\n"
      | _ ->
        let recvType = eventArgs.Metadata["type"]
        P ("Slave received invalid RpcPrimaryType",
           P $"Client: {config.machineName}",
           P $"Type value: {recvType}")
        |> config.log?SlaveErr
    with
    | ex -> 
      P ("RpcClient: Uncaught exception in MessageReceived",
         P $"Reason: {ex.Message}",
         P $"Stacktrace:\n{ex.StackTrace}")
      |> config.log?SlaveErr
