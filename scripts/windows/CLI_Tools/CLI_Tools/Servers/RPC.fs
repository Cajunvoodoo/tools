﻿module rec Bootstrap.Servers.RPC

open Newtonsoft.Json
open Bootstrap.Servers.Hardening.HardenableService
open Bootstrap.Servers.Hardening.Serivces
open System.Collections.Generic
open ES.Fslog
open Servers.Hardening.HardeningAction
open System.Text

// NB: RL = Remote Log
/// RPC identifiers for log messages. These are intended to be used as data in
/// a LogRpc to convert to the ES.Fslog LogLevel type (so we don't have to
/// serialize the library type w/ reflection).
type RpcLogLevel =
  | RLCritical = 0
  | RLErr = 1
  | RLWarn = 2
  | RLInfo = 3
  | RLVerbose = 4

let LL2RLL (ll: LogLevel): RpcLogLevel =
  match ll with
  | LogLevel.Critical -> RpcLogLevel.RLCritical
  | LogLevel.Error -> RpcLogLevel.RLErr
  | LogLevel.Warning -> RpcLogLevel.RLWarn
  | LogLevel.Informational -> RpcLogLevel.RLInfo
  | LogLevel.Verbose -> RpcLogLevel.RLVerbose
  | _ -> System.ArgumentOutOfRangeException() |> raise

/// There are two primary Rpc types: Control and Logging. Control is an action
/// that must be performed, and logging is self-explanatory. These are intended
/// to be used as Metadata in a packet.
type RpcPrimaryType =
  | Control = 0
  | Logging = 1
  | Hello = 2 // TODO: Handle Hello anywhere RpcPrimaryType is used

/// LogRpc is the type used by messages denoted by RpcPrimaryType's Logging type.
type LogRpc =
  | LogData of le: LogEvent
  | RpcData of rpc: ControlRpc * result: string

/// <summary>
/// Rpcs that can be sent to a Slave. Rpcs must be given to an IRpcAcceptor
/// and status updates are sent by the Slave to the Master (given to the
/// corresponding IRpcReceiver).
/// </summary>
/// <remarks>
/// Rpcs should be as small as possible, as the response must be (in the current
/// implementation) returned via metadata, which is computationally expensive.
/// Otherwise, the Master will be flooded with way too much data to parse, slowing
/// it to a crawl.
/// </remarks>
type ControlRpc =
  /// Print the name of the user running the Slave connection process.
  | Whoami
  /// Raw Powershell. Run as a script. You can figure it out.
  /// Note: This sets the ExecutionPolicy to bypass during execution, and back
  /// to what it was before execution after the command completes (or fails).
  | PowershellScript of script: string
  /// Run each line as an independent Powershell command, just as if you were
  /// using it yourself. This does not require changing the ExecutionPolicy.
  | PowershellRaw of command: string
  /// Enumerate the (non-standard) services on the system. If you want to disable
  /// a service, use the default Windows `sc` program to remotely disable the service.
  /// Or bug me to implement it. I don't know.
  /// A None filter means all will be shown. Some means only the provided Status
  /// will be returned.
  /// TODO: filter default Windows services
  | EnumerateServices of filter: Option<ServiceStatus>
  /// Enumerate the (non-standard) startup programs on the system.
  /// TODO: filter default Windows startup items
  | EnumerateStartupItems
  /// Set whether the specified startup item is active or not.
  | ModifyStartupItem of item: string * active: bool
  /// Enumerate the local users on the system, along with whether they are active.
  | EnumerateLocalUsers
  /// Set whether the specified local user account is active or not.
  /// TODO: Allow renaming local users?
  | ModifyLocalUser of user: string * active: bool
  /// Enumerate the running processes.
  /// TODO: Lable svchosts with the CLI arguments
  | EnumerateProcesses
  /// Kill the specified process. Please don't kill critical system services, as
  /// that will immediately result in a CRITICAL_PROCESS_DIED blue screen/bug check.
  /// TODO: Prevent KillProcess from killing critical processes, Just In Case.
  | KillProcess of name: string
  /// Enumerate the installed AD DS features, such as IIS, DNS, AD FS,
  /// Certificate Services, etc.
  | EnumerateInstalledADDSFeatures
  /// Apply a quick and dirty hardneing script for a specific service. You would
  /// use this when you need to add a service required by an inject (e.g., RDP or SSH),
  /// and want a quick and easy hardening script to perform. Note that some of
  /// these may change GPOs if applicable.
  | ApplyHardeningForService of service: HardenableService
  | ApplyHardeningAction of action: HardeningAction
  /// Pong.
  | Ping
  
/// A HelloRpc is the first message a Slave sends back to the Master. It includes
/// some basic information about the system that is easiest to express here.
type HelloRpc = {
  windowsVersion: string
  // TODO: what else should go in a HelloRpc?
}
  

/// Encode an Rpc (LogRpc, ControlRpc, or LogEvent) into a byte[], represented as JSON.
/// NOTE: we cannot use the JsonSerializer because it doesn't work with discriminated unions,
/// so we have to use a custom JSON serialization.
let EncodeRpc (rpc: 'T): byte[] =
  JsonConvert.SerializeObject rpc |> Encoding.Default.GetBytes


/// Decode an Rpc (LogRpc, ControlRpc, or LogEvent) that was encoded with EncodeRpc.
/// NOTE: we cannot use the JsonSerializer because it doesn't work with discriminated unions,
/// so we have to use a custom JSON serialization.
let DecodeRpc (data: byte[]): 'T =
  data
  |> Encoding.Default.GetString
  |> JsonConvert.DeserializeObject<'T> 

/// Create the metadata required to send an Rpc.
let CreateMetadata (name: string, rpcType: RpcPrimaryType): Dictionary<string, obj> =
  let metadata = new Dictionary<string, obj>()
  metadata.Add("name", name)
  metadata.Add("type", rpcType)
  metadata


/// <summary>
/// <para>
/// Sink for packets from the network client. This is used to handle packets
/// from the network client before the network client has been initialized.
/// For example, this could be used to handle logging information when the
/// connection to the Master has yet to be established.
/// </para>
/// <para>
/// The packets held in the sink can be flushed using the `Flush` method.
/// </para>
/// </summary>
type UninitializedNetworkClientPacketSink() =
  let mutable packets: LogRpc array = [||]
  let mutable conn: WatsonTcp.WatsonTcpClient option = None

  let sendPacket (c: WatsonTcp.WatsonTcpClient) (data: LogRpc) =
    // need name and type metadata
    let computerName = System.Environment.MachineName
    let encodedData = EncodeRpc(data)
    let metadata = CreateMetadata(computerName, RpcPrimaryType.Logging)
    c.SendAsync(encodedData, metadata) |> ignore

  member this.Send(packet: LogRpc) =
    match conn with
    | None -> packets <- Array.append packets [| packet |]
    | Some c -> sendPacket c packet

  member this.Flush(c: WatsonTcp.WatsonTcpClient) =
    conn <- Some c
    packets |> Array.iter (sendPacket c)
    packets <- [||]
